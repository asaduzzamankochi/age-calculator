package com.asaduzzamankochi.agecalculator;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private TextView txtCurrentDate;
    private TextView txtDateOfBirth;
    private TextView txtAge;
    private TextView txtNextBirthday;
    private int startDay = 1;
    private int startMonth = 12;
    private int startYear = 1980;
    public LinearLayout viewResult;

    private AgeCalculation age = new AgeCalculation();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCurrentDate = (TextView) findViewById(R.id.txtCurrentDate);
        txtCurrentDate.setText(age.getCurrentDate());
        txtDateOfBirth = (TextView) findViewById(R.id.txtDateOfBirth);
        //txtDateOfBirth.isClickable(true);
        txtAge = (TextView) findViewById(R.id.txtAge);
        txtNextBirthday = (TextView) findViewById(R.id.txtNextBirthday);
        viewResult = (LinearLayout) findViewById(R.id.viewResult);
        viewResult.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("CurrentDate", txtCurrentDate.getText().toString());
        savedInstanceState.putString("DateOfBirth", txtDateOfBirth.getText().toString());
        savedInstanceState.putString("Age", txtAge.getText().toString());
        savedInstanceState.putString("NextBirthday", txtNextBirthday.getText().toString());
        //viewResult.getVisibility();
        savedInstanceState.putInt("VISIBILITY", viewResult.getVisibility());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        txtCurrentDate.setText(savedInstanceState.getString("CurrentDate"));
        txtDateOfBirth.setText(savedInstanceState.getString("DateOfBirth"));
        txtAge.setText(savedInstanceState.getString("Age"));
        txtNextBirthday.setText(savedInstanceState.getString("NextBirthday"));
        if (savedInstanceState.getInt("VISIBILITY") != 0) {
            viewResult.setVisibility(View.INVISIBLE);
        } else {
            viewResult.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new DatePickerDialog(this,
                        mDateSetListener,
                        startYear, startMonth, startDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener
            = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            startYear = selectedYear;
            startMonth = selectedMonth;
            startDay = selectedDay;
            age.setDateOfBirth(startYear, startMonth, startDay);
            txtDateOfBirth.setText(selectedDay + "-" + (startMonth + 1) + "-" + startYear);

        }
    };

    public void birthdayView(View v) {
        //Toast.makeText(getBaseContext(), "click the resulted button" + age.getResult(), Toast.LENGTH_SHORT).show();
        showDialog(0);
    }

    public void calculate(View v) {
        calculateAge();
        viewResult.setVisibility(View.VISIBLE);
    }


    private void calculateAge() {
        age.calcualteYear();
        age.calcualteMonth();
        age.calcualteDay();
        //Toast.makeText(getBaseContext(), "click the resulted button" + age.getResult(), Toast.LENGTH_SHORT).show();
        txtAge.setText(age.getResult());
        txtNextBirthday.setText(age.dateLeft());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            about();
            return true;
        } else if (id == R.id.action_exit) {
            closeApp();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void about() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.app_name));
        alertDialogBuilder.setMessage(getString(R.string.about_me));
        alertDialogBuilder.setPositiveButton(getString(R.string.ok), null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void closeApp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.exit));
        alertDialogBuilder.setMessage(getString(R.string.want_to_exit));
        alertDialogBuilder.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                System.exit(0);
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.cancel), null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public void onBackPressed() {
        closeApp();
    }
}
